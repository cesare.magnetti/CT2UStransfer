import numpy as np
import scipy.ndimage
import argparse
import os
from os import listdir
from os.path import join
from PIL import Image 

eps = 1e-6
#define file formats that will work
IMG_EXTENSIONS = ['.nii.gz', '.nii', '.mha', '.mhd', '.png', '.jpg', '.jpeg', '.JPG', '.JPEG']
def _is_image_file(filename):
    return any(filename.endswith(extension) for extension in IMG_EXTENSIONS)

class SNR(object):
    def __call__(self, arr):
        """
        :param arr: <np.ndarray> : single image to check SNR for

                                -------
        :returns

            s2n : signal to noise ratio
        """
        m = arr.mean()
        sd = arr.std()
        return m/(sd+eps)

class VarLaplacian(object):
    def __init__(self, sigma = 1):
        '''
        :param sigma: wanted Gaussian std (the larger the smoother the output image)
        '''
        self.sigma = sigma

    def __call__(self, arr):

        """
        adapted from scipy.ndimage.gaussian_laplace

        :param tensor: <np.ndarray>: single image to check variance of Laplacian for

                                -------

        :returns: variance of Laplacian
        """
        return np.var(scipy.ndimage.gaussian_laplace(arr, sigma = self.sigma))


parser = argparse.ArgumentParser(description='evaluates some popular image quality metrics (SNR, Variance of Laplacian)\n'\
                                             'sample call: python get_metrics.py -f path-to-images -s path-to-save-directory')
parser.add_argument("--folder", '-f', type=str, help="path to folder(s) containing images for which we want to evaluate metrics.")

args = parser.parse_args()

if __name__=="__main__":
    # load all images
    filenames = [os.path.join(args.folder, f) for f in listdir(args.folder) if _is_image_file(join(args.folder, f))]
    images = [np.array(Image.open(f)) for f in filenames]
    # evaluate SNR and LAPV on all images
    snr, lapv = SNR(), VarLaplacian()
    SNRs, LAPVs = [], []
    for img in images:
        SNRs.append(snr(img))
        LAPVs.append(lapv(img))
    SNRs, LAPVs = np.array(SNRs), np.array(LAPVs)
    print("SNR:\t", SNRs.mean(), " +/- ", SNRs.std())
    print("LAPV:\t", LAPVs.mean(), " +/- ", LAPVs.std())

