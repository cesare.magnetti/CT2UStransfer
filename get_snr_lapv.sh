RESULTDIR=$1

# calculate FID for each model
echo -e "CycleGAN_standard:\t"
python3 ./get_metrics.py -f ${RESULTDIR}CycleGAN_standard/test_latest/images/fake_B
echo -e "\nCUT_standard:\t"
python3 ./get_metrics.py -f ${RESULTDIR}CUT_standard/test_latest/images/fake_B
echo -e "\nCycleGAN_noIdtLoss:\t"
python3 ./get_metrics.py -f ${RESULTDIR}CycleGAN_noIdtLoss/test_latest/images/fake_B
echo -e "\nCUT_noIdtLoss:\t"
python3 ./get_metrics.py -f ${RESULTDIR}CUT_noIdtLoss/test_latest/images/fake_B
echo -e "\nCycleGAN_LPIPS:\t"
python3 ./get_metrics.py -f ${RESULTDIR}CycleGAN_LPIPS/test_latest/images/fake_B
echo -e "\nCycleGAN_LPIPS_noIdtLoss:\t"
python3 ./get_metrics.py -f ${RESULTDIR}CycleGAN_LPIPS_noIdtLoss/test_latest/images/fake_B
echo -e "\nCycleGAN_LPIPS_noIdtLoss_lambda_AB_1:\t"
python3 ./get_metrics.py -f ${RESULTDIR}CycleGAN_LPIPS_noIdtLoss_lambda_AB_1/test_latest/images/fake_B
echo -e "\nCUT_standard_noLayer0:\t"
python3 ./get_metrics.py -f ${RESULTDIR}CUT_standard_noLayer0/test_latest/images/fake_B
echo -e "\nCUT_noIdtLoss_noLayer0:\t"
python3 ./get_metrics.py -f ${RESULTDIR}CUT_noIdtLoss_noLayer0/test_latest/images/fake_B